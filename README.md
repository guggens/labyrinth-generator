# labyrinth-generator

Ferienproject mit meinem Sohn - Labyrinth-Generator in Logo

Wir haben den folgenden Turtle Coder verwendet: https://www.code-your-life.org/turtlecoder/

In ungefähr drei Stunden haben wir das Ding zum Laufen gebracht, orientiert an folgendem Blog:

http://weblog.jamisbuck.org/2011/1/12/maze-generation-recursive-division-algorithm

Leider ist der TurtleCoder dabei an seine Grenzen gestossen, nach einigem Refactoring und code-vereinfachung liess es sich aber zumindest ausführen. 

Code siehe logo-code, einfach in den TurtleCoder kopieren, Gangbreite und grösse einstellen und los gehts!